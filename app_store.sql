-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 04:51 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `modulo`
--

CREATE TABLE `modulo` (
  `id` int(11) NOT NULL,
  `id_tipo_modulo` int(11) DEFAULT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` longtext NOT NULL,
  `forma_pagamento` varchar(100) NOT NULL,
  `icon` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modulo`
--

INSERT INTO `modulo` (`id`, `id_tipo_modulo`, `titulo`, `descricao`, `forma_pagamento`, `icon`) VALUES
(1, 1, 'Checkin & Checkout ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl aliquam, eleifend augue non, gravida eros. Donec gravida suscipit placerat. Pellentesque vel ultricies eros, nec molestie risus. Integer volutpat at justo vitae vehicula. In hac habitasse platea dictumst. Aliquam tincidunt, dui dapibus auctor pretium, odio lacus egestas velit, et fringilla felis orci nec felis. In sed laoreet purus. Morbi maximus pretium turpis vitae eleifend. Quisque sit amet tellus ex. Aenean efficitur, ex sed condimentum eleifend, lacus elit condimentum massa, quis fringilla felis diam nec turpis. Suspendisse blandit tincidunt ante et convallis.', '+ R$ X por colaborador por mês', '01.png'),
(2, 2, 'Badges & Cards de Reconhecimento', 'Sed cursus tortor luctus nisl aliquet, ut sagittis elit commodo. Pellentesque efficitur luctus molestie. Quisque vulputate nec ligula eget pretium. In et sollicitudin velit. Ut facilisis, augue sit amet convallis efficitur, lectus magna facilisis sem, ac ullamcorper justo quam eget dolor. Phasellus eu ornare lorem. Nullam sed sem tempus, ornare est at, consectetur enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque vulputate ante lorem, at aliquam velit lobortis eget. Nunc sodales neque sed congue mollis.', 'Gratuito', '02.png'),
(3, 2, 'Planos de Ação', 'Nulla in venenatis ligula. Proin velit sem, laoreet vel finibus eget, eleifend at risus. Nullam ultrices placerat augue, sed blandit dolor luctus quis. Aenean commodo felis metus, venenatis faucibus felis accumsan ac. In sed semper eros. Vivamus ac dolor sit amet risus aliquet vestibulum. Mauris dictum libero vel felis varius pulvinar. In hac habitasse platea dictumst. Vestibulum dignissim et velit ut luctus.', 'Gratuito', '03.png'),
(4, 2, 'Recomendações de Conteúdo sob medida', 'Proin arcu lacus, mattis in aliquet at, faucibus eget dui. Vestibulum a mi sed orci luctus ornare. Cras justo velit, sollicitudin sit amet hendrerit at, laoreet vitae nibh. Ut id faucibus metus. Cras molestie metus libero, eu elementum nunc viverra id. Cras ultricies nunc ut mattis suscipit. Nunc id eros neque. Fusce ac mauris tellus. Integer non porta enim, vitae fringilla arcu. Ut nisi risus, vehicula quis gravida nec, sollicitudin vel ipsum.', '+ R$ X por colaborador por mês', '04.png'),
(5, 3, 'Fitting Cultural', 'Nullam vel mauris in turpis tristique ullamcorper eget a ex. Pellentesque nunc odio, maximus quis bibendum sit amet, sollicitudin in massa. Donec consequat luctus dolor, nec scelerisque nulla feugiat commodo. Phasellus egestas sed sapien vel ultricies. Quisque vel semper lacus. Proin ut risus nulla. Integer ultricies aliquet nisi, id cursus nisi pulvinar ut.', '+ R$ X por colaborador por mês', '05.png'),
(6, 4, 'Assessments e Recomendações', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra metus mattis magna tempor euismod. Curabitur massa nulla, porta vel aliquet in, luctus pharetra nisi. Donec commodo eget mi vel accumsan. Proin efficitur pharetra quam, et laoreet lectus lacinia nec. Ut auctor ante at justo egestas, eget mollis magna congue. Sed fermentum metus in lacus commodo, nec accumsan justo hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus quis ligula justo.', 'Gratuito', '06.png'),
(7, 1, 'Quick Deck', 'Proin commodo ante sed nisi iaculis, aliquam tempus velit laoreet. Phasellus auctor venenatis gravida. Ut eu dolor ipsum. Suspendisse vitae bibendum leo. Phasellus quis condimentum lacus, vel cursus massa. Phasellus vitae magna tincidunt, tristique dui ut, tristique risus. Nunc suscipit aliquet sem vel molestie. Suspendisse a nulla commodo diam efficitur pellentesque non vitae tellus. Sed quis condimentum mi, a ultrices erat. Donec pretium ligula nec sapien venenatis, id congue orci faucibus.', '+ R$ X por colaborador por mês', '07.png'),
(8, 1, 'Evidências & Feedback', 'Ut consectetur purus ut enim eleifend, et rutrum ex egestas. Donec non commodo sapien, eu tempus nulla. Donec venenatis auctor lacus vel tincidunt. Sed sodales mi in tempor condimentum. Nullam sit amet est in augue feugiat molestie sit amet non odio. Nunc dui purus, venenatis sit amet nulla non, dapibus dignissim lorem. Vivamus sit amet nisl convallis ex eleifend sollicitudin. Morbi tincidunt aliquam lectus quis rhoncus. Proin a nibh viverra, gravida justo consectetur, sollicitudin magna. Aenean vel laoreet diam. Curabitur eu enim orci. Aenean sed magna cursus, aliquam lorem sit amet, lacinia lectus. Maecenas odio dolor, elementum ac efficitur sit amet, tincidunt at mi. Donec ultrices ex ante, id hendrerit nunc pellentesque nec. Nullam imperdiet ultricies ligula, vel consectetur ipsum blandit consequat. Nam consequat aliquet lacus quis pulvinar.', '+ R$ X por colaborador por mês', '08.png');

-- --------------------------------------------------------

--
-- Table structure for table `modulo_ativo`
--

CREATE TABLE `modulo_ativo` (
  `id` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `desscricao` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `desscricao`) VALUES
(1, 'Ativo'),
(2, 'Desabilitado');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_modulo`
--

CREATE TABLE `tipo_modulo` (
  `id` int(11) NOT NULL,
  `descricao` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_modulo`
--

INSERT INTO `tipo_modulo` (`id`, `descricao`) VALUES
(1, 'Performance'),
(2, 'Engajamento'),
(3, 'Cultura'),
(4, 'Relações');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_modulo_id_tipo_modulo_idx` (`id_tipo_modulo`);

--
-- Indexes for table `modulo_ativo`
--
ALTER TABLE `modulo_ativo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_modulo_ativo_status_idx` (`id_status`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_modulo`
--
ALTER TABLE `tipo_modulo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `modulo_ativo`
--
ALTER TABLE `modulo_ativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo_modulo`
--
ALTER TABLE `tipo_modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `modulo`
--
ALTER TABLE `modulo`
  ADD CONSTRAINT `fk_modulo_id_tipo_modulo` FOREIGN KEY (`id_tipo_modulo`) REFERENCES `tipo_modulo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `modulo_ativo`
--
ALTER TABLE `modulo_ativo`
  ADD CONSTRAINT `fk_modulo_ativo_status` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
