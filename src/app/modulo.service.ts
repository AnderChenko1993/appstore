import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModuloService {
  API_URL: string = 'http://localhost:8888';

  constructor(private http: HttpClient) { }

  lista(): Observable<any> {
    return this.http.get(`${this.API_URL}/modulo/lista`);
  }

  detalhe(id): Observable<any> {
    return this.http.get(`${this.API_URL}/modulo/detalhe/${id}`);
  }

  alteraStatus(id): Observable<any> {      
    let body = new FormData();
    body.append('id', id);

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'x-www-form-urlencoded');        
    return this.http.post(`${this.API_URL}/modulo/alterar-status`, body, { headers: headers });
  }
}
