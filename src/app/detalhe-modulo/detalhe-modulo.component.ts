import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import { ModuloService } from '../modulo.service';

@Component({
  selector: 'app-detalhe-modulo',
  templateUrl: './detalhe-modulo.component.html',
  styleUrls: ['./detalhe-modulo.component.css']
})
export class DetalheModuloComponent implements OnInit {
  id: number;
  modulo: any;  
  bgColors = { 1: '#3366ff', 2: '#990099', 3: '#248f24', 4: '#e65c00' };

  constructor(private moduloService: ModuloService, 
              private activateRoute: ActivatedRoute,
              private router: Router,
            ) 
  {
      this.activateRoute.params.subscribe( params => {
        this.id = params['id']; 
        this.detalhe();
      });
   }

  ngOnInit() {}

  detalhe() {
    this.moduloService.detalhe(this.id).subscribe(modulo => {
      this.modulo = modulo;
    }, error => {
      alert(error.message);
    });
  }

  alteraStatus(status) {    
    this.moduloService.alteraStatus(this.id).subscribe(result => {
      if(result) 
        this.router.navigate(['/']);  
      else 
        alert('Erro executar ação, por favor, tente novamente!');      
    }, error => {
      alert(error.message);
    });
  }

}
