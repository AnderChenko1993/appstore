import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheModuloComponent } from './detalhe-modulo.component';

describe('DetalheModuloComponent', () => {
  let component: DetalheModuloComponent;
  let fixture: ComponentFixture<DetalheModuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheModuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
