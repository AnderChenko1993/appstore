import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import { ModuloService } from '../modulo.service';

@Component({
  selector: 'app-lista-modulos',
  templateUrl: './lista-modulos.component.html',
  styleUrls: ['./lista-modulos.component.css']
})
export class ListaModulosComponent implements OnInit {
  modulos: any;
  acao: number;
  bgColors = { 1: '#3366ff', 2: '#990099', 3: '#248f24', 4: '#e65c00' };

  constructor(private moduloService: ModuloService, private router: Router) {}

  ngOnInit() {
    this.getModulos();
  }

  getModulos() {
    this.moduloService.lista().subscribe(modulos => {
      this.modulos = modulos;
    }, error => {
      alert(error.message);
    });
  }

  alteraStatus(event, id) {    
    let acao = (<HTMLInputElement>event.target).innerHTML;
    if(acao == 'Contratar' || acao == 'Ativar') {
      (<HTMLInputElement>event.target).innerHTML = 'Desativar';
    }
    else {
      (<HTMLInputElement>event.target).innerHTML = 'Ativar';
    }
    
    this.moduloService.alteraStatus(id).subscribe(result => {
      if(!result) {
        alert('Erro executar ação, por favor, tente novamente!');      
      }
    }, error => {
      alert(error.message);
    });
  }
}
