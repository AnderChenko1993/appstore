import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaModulosComponent } from './lista-modulos/lista-modulos.component';
import { DetalheModuloComponent } from './detalhe-modulo/detalhe-modulo.component';

const routes: Routes = [
  { path: '', component: ListaModulosComponent },
  { path: 'detalhe-modulo/:id', component: DetalheModuloComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ListaModulosComponent, DetalheModuloComponent];
