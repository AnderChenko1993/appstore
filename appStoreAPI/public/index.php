<?php
 
require 'vendor/autoload.php';
require 'bootstrap/app.php';
 
$app = new Slim\App();

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$container = $app->getContainer();
$container['ModuloController'] = function($container) use($app) {
    return new App\Controllers\ModuloController();
};

$app->get('/modulo/lista', 'ModuloController:lista');
$app->get('/modulo/detalhe/{id}', 'ModuloController:detalhe');
$app->post('/modulo/alterar-status', 'ModuloController:alteraStatus');
 
$app->run();