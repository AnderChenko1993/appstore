<?php 

namespace App\Controllers;

use App\Models\Modulo;
use App\Models\ModuloAtivo;

class ModuloController 
{
    public function lista($request, $response) 
    {
        $modulos = Modulo::with('tipoModulo')
                    ->with('moduloAtivo')
                    ->get();

        $data = json_encode($modulos);
        $response->getBody()->write($data);
        return $response;
    }

    public function detalhe($request, $response, $args) 
    {
        $modulo = Modulo::with('tipoModulo')
                    ->with('moduloAtivo')
                    ->find($args['id']);                    

        $data = json_encode($modulo);        
        $response->getBody()->write($data);
        return $response;
    }

    public function alteraStatus($request, $response) {   
        $data = $request->getParsedBody();

        try {  
            $modulo = ModuloAtivo::where('id_modulo', $data['id'])->first();
            if(isset($modulo)) {

                if($modulo->id_status == 1) {
                    $modulo->id_status = 2;    
                    $modulo->save();
                }
                else {
                    $modulo->id_status = 1;
                    $modulo->save();
                }
            }
            else {
                $modulo = new ModuloAtivo;
                $modulo->id_modulo = $data['id'];
                $modulo->id_status = 1;
                $modulo->save();
            }

            
            // if($data['status'] == 1 || $data['status'] == 2) {
            //     $modulo = ModuloAtivo::where('id_modulo', $data['id'])->first();                
            //     $modulo->id_status = $data['status'];
            //     $modulo->save();
            // }
            // else {
            //     $modulo = new ModuloAtivo;
            //     $modulo->id_modulo = $data['id'];
            //     $modulo->id_status = 1;
            //     $modulo->save();
            // }

            $response->getBody()->write(true);
        } catch(\Exception $e) {
            $response->getBody()->write("Erro ao executar ação! '(".$e->getMessage().")'");
        }

        return $response;        
    }
}