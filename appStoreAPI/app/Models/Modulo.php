<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model {
    protected $table = 'modulo';   

    public function tipoModulo() {
        return $this->belongsTo('\App\Models\TipoModulo', 'id_tipo_modulo', 'id');
    }

    public function moduloAtivo() {
        return $this->belongsTo('\App\Models\ModuloAtivo', 'id', 'id_modulo');
    }
}