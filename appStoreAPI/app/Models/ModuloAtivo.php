<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuloAtivo extends Model {
    protected $table = 'modulo_ativo';   
    protected $fillable = [
        'id_modulo',
        'id_status',        
    ];
}